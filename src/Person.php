<?php

namespace Faker\Ukraine;

use Faker\Extension\Extension;

class Person extends \Faker\Provider\uk_UA\Person implements Extension
{
    /**
     * Individual Identification Number
     *
     * @param \DateTime $birthdate Date when person is born
     * @param string    $sex       'male' for male or 'female' for female
     *
     * @link https://en.wikipedia.org/wiki/National_identification_number#Ukraine
     * @link https://github.com/iiifx-production/ukraine-identification-number Check digit calculation algorithm
     *
     * @return string 10 digit number, for example: 34713401358
     */
    public function individualIdentificationNumber($birthdate = null, $sex = null)
    {
        $birthdate = $birthdate ?: \Faker\Provider\DateTime::dateTimeThisCentury();
        $sex = $sex ?: self::randomElement([static::GENDER_MALE, static::GENDER_FEMALE]);

        // real life examples shows that the birth date itself is also included in counting the days from 1900-01-01
        $iid = str_pad($birthdate->diff(new \DateTime('1900-01-01'))->days + 1, 5, '0', STR_PAD_LEFT);
        $iid .= static::numerify('###');
        $iid .= ($sex == static::GENDER_MALE ? static::numberBetween(0, 4) * 2 + 1 : static::numberBetween(0, 4) * 2);

        $weights = [-1, 5, 7, 9, 4, 6, 10, 5, 7];
        $checksum = 0;
        for ($i = 0; $i < count($weights); $i++) {
            $checksum += $weights[$i] * $iid[$i];
        }
        $checksum = $checksum % 11 % 10;
        $iid .= $checksum;

        return $iid;
    }
}
