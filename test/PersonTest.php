<?php

namespace Faker\Test\Ukraine;

use Faker\Ukraine\Factory;
use Faker\Ukraine\Person;
use PHPUnit\Framework\TestCase;

class PersonTest extends TestCase
{
    public function testIndividualIdentificationNumberReturnLength()
    {
        $faker = Factory::ukraine();
        $faker->seed(1);

        $this->assertEquals(10, strlen($faker->IndividualIdentificationNumber()));
    }

    public function testIndividualIdentificationNumberWithBirthday()
    {
        $faker = Factory::ukraine();
        $faker->seed(1);

        $this->assertEquals('3476100763', $faker->IndividualIdentificationNumber(new \DateTime('1995-03-04')));
    }

    public function testIndividualIdentificationNumberFemale()
    {
        $faker = Factory::ukraine();
        $faker->seed(1);
        $this->assertEquals('3476157905', $faker->IndividualIdentificationNumber(new \DateTime('1995-03-04'), Person::GENDER_FEMALE));
    }

    public function testIndividualIdentificationNumberMale()
    {
        $faker = Factory::ukraine();
        $faker->seed(1);
        $this->assertEquals('3476157911', $faker->IndividualIdentificationNumber(new \DateTime('1995-03-04'), Person::GENDER_MALE));
    }

    public function testReallyOldPersonIndividualIdentificationNumberLength()
    {
        $faker = Factory::ukraine();
        $faker->seed(1);
        $this->assertEquals('0188900764', $faker->IndividualIdentificationNumber(new \DateTime('1905-03-04')));
    }
}
